import React, { Component } from "react";
import ListShoe from "./ListShoe";
import GioHang from "./GioHang";
import DetailShoe from "./DetailShoe";

export default class Ex_ShoeShop_Redux extends Component {
  render() {
    return (
      <div className="mb-5">
        <GioHang />
        <ListShoe />
        <DetailShoe />
      </div>
    );
  }
}
