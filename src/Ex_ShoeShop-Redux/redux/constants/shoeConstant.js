export const showDetail = "showDetail";
export const addToCart = "addToCart";
export const changeAmount = "changeAmount";
export const removeFromCart = "removeFromCart";
