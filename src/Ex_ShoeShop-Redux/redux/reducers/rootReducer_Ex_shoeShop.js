import { combineReducers } from "redux";
import { dataShoe_reducer } from "./Data_Shoe_reducer";
import { detailShoe_reducer } from "./detailShoe_reducer";
import { gioHangReducer } from "./gioHangReducer";

export let rootReducer_Ex_shoeShop = combineReducers({
  shoeListReducer: dataShoe_reducer,
  detailShoeReducer: detailShoe_reducer,
  gioHangReducer,
});
