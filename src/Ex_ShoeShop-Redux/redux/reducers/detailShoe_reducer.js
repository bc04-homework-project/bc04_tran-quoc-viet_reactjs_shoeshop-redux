import { showDetail } from "../constants/shoeConstant";

let detailShoe = "";

export let detailShoe_reducer = (state = detailShoe, action) => {
  if (action.type == showDetail) {
    state = action.payload;
    return state;
  }

  return state;
};
