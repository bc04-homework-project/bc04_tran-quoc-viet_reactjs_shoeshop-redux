import {
  addToCart,
  changeAmount,
  removeFromCart,
} from "../constants/shoeConstant";

let stateGioHang = [];

export let gioHangReducer = (state = stateGioHang, { type, payload }) => {
  switch (type) {
    case addToCart: {
      let index = state.findIndex((item) => {
        return item.id == payload.id;
      });
      // console.log('index: ', index);

      if (index == -1) {
        let spGioHang = { ...payload, soLuong: 1 };
        // console.log("spGioHang: ", spGioHang);
        state.push(spGioHang);
        return [...state];
      } else {
        state[index].soLuong++;
        return [...state];
      }
    }

    case changeAmount: {
      let index = state.findIndex((item) => {
        return item.id == payload.shoeID;
      });
      if (payload.value) {
        state[index].soLuong++;
        return [...state];
      } else {
        if (state[index].soLuong > 1) {
          state[index].soLuong--;
          return [...state];
        }
        return [...state];
      }
    }

    case removeFromCart: {
      let index = state.findIndex((item) => {
        return item.id == payload;
      });
      state.splice(index, 1);
      return [...state];
    }

    default: {
      return [...state];
    }
  }
};
