import React, { Component } from "react";
import { connect } from "react-redux";
import { addToCart, showDetail } from "./redux/constants/shoeConstant";

class ItemShoe extends Component {
  render() {
    let { name, image, price, shortDescription } = this.props.detail;

    return (
      <div>
        <div className="card">
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p class="card-text">{shortDescription}</p>
            <p href="#" className="btn btn-primary">
              ${price}
            </p>
            <p>
              <button
                onClick={() => this.props.handleAddToCart(this.props.detail)}
                className="btn btn-success"
              >
                Add to cart
              </button>
              <button
                data-toggle="modal"
                data-target="#myModal"
                onClick={() => this.props.handleDetail(this.props.detail)}
                className="btn btn-warning"
              >
                More detail
              </button>
            </p>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleDetail: (shoe) => {
      let action = {
        type: showDetail,
        payload: shoe,
      };
      dispatch(action);
    },
    handleAddToCart: (shoe) => {
      let action = {
        type: addToCart,
        payload: shoe,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
