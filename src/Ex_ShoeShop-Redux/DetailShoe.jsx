import React, { Component } from "react";
import { connect } from "react-redux";

class DetailShoe extends Component {
  render() {
    let { image, name, price, description, quantity } = this.props.detailShoe;
    return (
      <div className="modal fade p-5" id="myModal">
        <div className="modal-dialog bg-light p-5 rounded-lg">
          <div>
            <img src={image} className="w-100" />
          </div>
          <div className="text-primary text-left border border-info rounded-lg p-2">
            <p>
              <span className="text-danger">Name:</span> {name}
            </p>
            <p>
              <span className="text-danger">Price:</span> ${price}
            </p>
            <p>
              <span className="text-danger">Description:</span> {description}
            </p>
            <p>
              <span className="text-danger">Stocking:</span> {quantity}
            </p>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detailShoe: state.detailShoeReducer,
  };
};

export default connect(mapStateToProps)(DetailShoe);
