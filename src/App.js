import "./App.css";
import Ex_ShoeShop_Redux from "./Ex_ShoeShop-Redux/Ex_ShoeShop";

function App() {
  return (
    <div className="App">
      <Ex_ShoeShop_Redux />
    </div>
  );
}

export default App;
